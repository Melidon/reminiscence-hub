import "dart:async";
import "dart:io";

import "package:chopper/chopper.dart" hide Authenticator;
import "package:chopper/chopper.dart" as chopper show Authenticator;
import "package:localstorage/localstorage.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/services/auth.dart";

class Backend {
  static Openapi get instance => _openapi;
  static final _openapi = Openapi.create(
    baseUrl: Uri(
      scheme: "http",
      host: "vm.niif.cloud.bme.hu",
      port: 4916,
    ),
    interceptors: [authInterceptor],
    authenticator: Authenticator(),
  );
}

FutureOr<Request> authInterceptor(Request request) async {
  final token = await switch (request.url.path) {
    "/api/v1/users/refresh" => AuthRepository.instance.getRefreshToken(),
    _ => AuthRepository.instance.getAccessToken(),
  };
  if (token == null) {
    return request;
  }
  final updatedRequest = applyHeader(
    request,
    HttpHeaders.authorizationHeader,
    "Bearer $token",
  );
  return updatedRequest;
}

class Authenticator extends chopper.Authenticator {
  Authenticator();

  static const retryCountHeader = "Retry-Count";
  static const maxRetryCount = 1;

  @override
  FutureOr<Request?> authenticate(
    Request request,
    Response response, [
    Request? originalRequest,
  ]) async {
    if (response.statusCode == HttpStatus.unauthorized) {
      final retryCount = int.tryParse(request.headers[retryCountHeader] ?? "0") ?? 0;
      if (retryCount >= maxRetryCount) {
        return null;
      }
      try {
        await Auth.instance.refresh();
        final newAccessToken = await AuthRepository.instance.getAccessToken();
        if (newAccessToken == null) {
          return null;
        }
        return applyHeaders(
          request,
          {
            HttpHeaders.authorizationHeader: "Bearer $newAccessToken",
            retryCountHeader: (retryCount + 1).toString(),
          },
        );
      } catch (error) {
        return null;
      }
    }
    return null;
  }
}

class AuthRepository {
  static AuthRepository get instance => _instance;
  static final AuthRepository _instance = AuthRepository._();
  AuthRepository._();

  static const _key = "value";
  final LocalStorage _accessTokenStorage = LocalStorage("accessToken.json");
  final LocalStorage _refreshTokenStorage = LocalStorage("refreshToken.json");

  Future<String?> getAccessToken() async {
    await _accessTokenStorage.ready;
    return _accessTokenStorage.getItem(_key);
  }

  Future<void> setAccessToken(String? accessToken) async {
    await _accessTokenStorage.ready;
    if (accessToken == null) {
      _accessTokenStorage.deleteItem(_key);
    } else {
      _accessTokenStorage.setItem(_key, accessToken);
    }
  }

  Future<String?> getRefreshToken() async {
    await _refreshTokenStorage.ready;
    return _refreshTokenStorage.getItem(_key);
  }

  Future<void> setRefreshToken(String? refreshToken) async {
    await _refreshTokenStorage.ready;
    if (refreshToken == null) {
      _refreshTokenStorage.deleteItem(_key);
    } else {
      _refreshTokenStorage.setItem(_key, refreshToken);
    }
  }
}
