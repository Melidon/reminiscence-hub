import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/services/database.dart";
import "package:reminiscence_hub/utility.dart";

class NotesView extends StatefulWidget {
  const NotesView({super.key});

  @override
  State<NotesView> createState() => _NotesViewState();
}

class _NotesViewState extends State<NotesView> {
  final _notesFuture = Database.instance.notes.getAll();
  var _notes = <NoteTDO>[];

  Future<void> _showAddDialog(BuildContext context) async {
    final noteController = TextEditingController();
    final noteBody = await showDialog<NoteBody>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Add Note"),
          content: TextField(
            controller: noteController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Note",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop<NoteBody>(context, null),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop<NoteBody>(context, NoteBody(text: noteController.text)),
              child: const Text("Add"),
            ),
          ],
        );
      },
    );
    if (noteBody == null) {
      return;
    }
    try {
      final noteTDO = await Database.instance.notes.add(noteBody);
      setState(() => _notes.add(noteTDO));
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to add note");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  Future<void> _showEditDialog(BuildContext context, int index) async {
    final noteTDO = _notes[index];
    final noteController = TextEditingController();
    noteController.text = noteTDO.text;
    final noteBody = await showDialog<NoteBody>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Edit Note"),
          content: TextField(
            controller: noteController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Note",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop<NoteBody>(context, null),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop<NoteBody>(context, NoteBody(text: noteController.text)),
              child: const Text("Edit"),
            ),
          ],
        );
      },
    );
    if (noteBody == null) {
      return;
    }
    try {
      final updatedNoteTDO = await Database.instance.notes.update(noteTDO.id, noteBody);
      setState(() => _notes[index] = updatedNoteTDO);
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to edit note");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  Future<void> _showDeleteDialog(BuildContext context, int index) async {
    final noteTDO = _notes[index];
    final result = await showDialog<bool>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Delete Note"),
          content: Text("Are you sure you want to delete ${noteTDO.text}?"),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop<bool>(context, false),
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () => Navigator.pop<bool>(context, true),
              child: const Text("Delete"),
            ),
          ],
        );
      },
    );
    if (result == null || !result) {
      return;
    }
    try {
      await Database.instance.notes.delete(noteTDO.id);
      setState(() => _notes.removeAt(index));
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to delete note");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _notesFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.hasError) {
          return Center(child: Text("${snapshot.error}"));
        }
        _notes = snapshot.data!;
        return Scaffold(
          body: ListView.builder(
            itemBuilder: (context, index) {
              final noteTDO = _notes[index];
              return ListTile(
                title: Text(noteTDO.text),
                trailing: PopupMenuButton(
                  itemBuilder: (context) {
                    return [
                      const PopupMenuItem(
                        value: "edit",
                        child: Text("Edit"),
                      ),
                      const PopupMenuItem(
                        value: "delete",
                        child: Text("Delete"),
                      ),
                    ];
                  },
                  onSelected: (value) {
                    switch (value) {
                      case "edit":
                        _showEditDialog(context, index);
                      case "delete":
                        _showDeleteDialog(context, index);
                    }
                  },
                ),
              );
            },
            itemCount: _notes.length,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => _showAddDialog(context),
            child: const Icon(Icons.note_add),
          ),
        );
      },
    );
  }
}
