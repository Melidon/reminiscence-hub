import "package:flutter/material.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/views/sign_in_screen.dart";

class AuthGate extends StatelessWidget {
  final Widget child;

  const AuthGate({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: Auth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == false) {
          return const SignInScreen();
        }
        return child;
      },
    );
  }
}
