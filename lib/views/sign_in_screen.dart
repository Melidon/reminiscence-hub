import "package:email_validator/email_validator.dart";
import "package:flutter/material.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/utility.dart";

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  bool isSignInMode = true;

  @override
  Widget build(BuildContext context) {
    final emailController = TextEditingController();
    final passwordController = TextEditingController();
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                isSignInMode ? "Sign in" : "Register",
                style: const TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 16.0),
              Row(
                children: [
                  Text(isSignInMode ? "Don't have an account? " : "Already have an account? "),
                  TextButton(
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
                    ),
                    onPressed: () {
                      setState(() {
                        isSignInMode = !isSignInMode;
                      });
                    },
                    child: Text(isSignInMode ? "Register" : "Sign in"),
                  ),
                ],
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                controller: emailController,
                decoration: const InputDecoration(
                  labelText: "Email",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Email is required";
                  }
                  if (!EmailValidator.validate(value)) {
                    return "Email is not valid";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                controller: passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: "Password",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Password is required";
                  }
                  // TODO: Check password strength
                  return null;
                },
              ),
              if (!isSignInMode) const SizedBox(height: 16.0),
              if (!isSignInMode)
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: "Confirm Password",
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value != passwordController.text) {
                      return "Passwords do not match";
                    }
                    return null;
                  },
                ),
              const SizedBox(height: 16.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      String email = emailController.text;
                      String password = passwordController.text;
                      if (isSignInMode) {
                        try {
                          await Auth.instance.signInWithEmailAndPassword(email, password);
                        } catch (exception) {
                          if (context.mounted) {
                            final message = messageFromError(exception, "Sign in failed");
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  message,
                                  style: TextStyle(color: Theme.of(context).colorScheme.error),
                                ),
                              ),
                            );
                          }
                        }
                      } else {
                        try {
                          await Auth.instance.createUserWithEmailAndPassword(email, password);
                        } catch (exception) {
                          if (context.mounted) {
                            final message = messageFromError(exception, "Registration failed");
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                content: Text(
                                  message,
                                  style: TextStyle(color: Theme.of(context).colorScheme.error),
                                ),
                              ),
                            );
                          }
                        }
                      }
                    }
                  },
                  child: Text(isSignInMode ? "Sign in" : "Register"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
