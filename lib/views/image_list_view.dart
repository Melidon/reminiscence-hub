import "dart:typed_data";

import "package:flutter/material.dart";
import "package:image_picker/image_picker.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/services/database.dart";
import "package:reminiscence_hub/utility.dart";
import 'package:reminiscence_hub/views/image_details_view.dart';

class ImageListView extends StatefulWidget {
  const ImageListView({super.key});

  @override
  State<ImageListView> createState() => _ImageListViewState();
}

class _ImageListViewState extends State<ImageListView> {
  final _imageMetaListFuture = Database.instance.images.getAllMeta();
  List<ImageMetaTDO>? _imageMetaList;
  final Map<String, Future<Uint8List>> _imageBytesFutureMap = {};
  final Map<String, Uint8List> _imageBytesMap = {};

  Future<void> _addImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    try {
      final imageMeta = await Database.instance.images.add(pickedFile);
      setState(() {
        _imageMetaList!.add(imageMeta);
        _imageBytesFutureMap[imageMeta.id] = Database.instance.images.get(imageMeta);
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to add ${pickedFile.name}");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  Future<void> _deleteImage(ImageMetaTDO imageMeta) async {
    final result = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("Delete image"),
        content: Text("Are you sure you want to delete ${imageMeta.imageName}?"),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(false),
            child: const Text("Cancel"),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(true),
            child: const Text("Delete"),
          ),
        ],
      ),
    );
    if (result == null || !result) {
      return;
    }
    try {
      await Database.instance.images.delete(imageMeta);
      setState(() {
        _imageMetaList!.remove(imageMeta);
        _imageBytesFutureMap.remove(imageMeta.id);
        _imageBytesMap.remove(imageMeta.id);
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to delete ${imageMeta.imageName}");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _imageMetaListFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.hasError) {
          final message = messageFromError(snapshot.error, "Failed to load images");
          return Center(
            child: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          );
        }
        if (_imageMetaList == null) {
          _imageMetaList = snapshot.data!;
          for (final imageMeta in _imageMetaList!) {
            _imageBytesFutureMap[imageMeta.id] = Database.instance.images.get(imageMeta);
          }
        }
        return Scaffold(
          body: ListView.builder(
            itemBuilder: (context, index) {
              final imageMeta = _imageMetaList![index];
              return ListTile(
                leading: Hero(
                  tag: imageMeta.id,
                  child: FutureBuilder(
                    future: _imageBytesFutureMap[imageMeta.id],
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return const CircularProgressIndicator();
                      }
                      if (snapshot.hasError) {
                        return const Icon(Icons.error);
                      }
                      final imageBytes = snapshot.data!;
                      _imageBytesMap[imageMeta.id] = imageBytes;
                      return Image.memory(
                        imageBytes,
                        width: 96,
                      );
                    },
                  ),
                ),
                title: Text(imageMeta.imageName),
                subtitle: Text("id: ${imageMeta.id}"),
                trailing: IconButton(
                  onPressed: () async => await _deleteImage(imageMeta),
                  tooltip: "Delete image",
                  icon: const Icon(Icons.delete),
                ),
                onTap: () async {
                  final image = await decodeImageFromList(_imageBytesMap[imageMeta.id]!);
                  // ignore: use_build_context_synchronously
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ImageDetailsView(imageMeta, image),
                    ),
                  );
                },
              );
            },
            itemCount: _imageMetaList!.length,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: _addImage,
            tooltip: "Add image",
            child: const Icon(Icons.add_a_photo),
          ),
        );
      },
    );
  }
}
