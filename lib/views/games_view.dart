import "dart:math" show Random;
import "dart:ui" as ui show Image;
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/image_annotator.dart";
import "package:reminiscence_hub/services/database.dart";
import "package:reminiscence_hub/utility.dart";

class GamesView extends StatefulWidget {
  const GamesView({super.key});

  @override
  State<GamesView> createState() => _GamesViewState();
}

class _GamesViewState extends State<GamesView> {
  static Future<(List<ImageMetaTDO>, List<NoteTDO>)> _loadData() async {
    final imageMetaTDOsFuture = Database.instance.images.getAllMeta();
    final noteTODsFuture = Database.instance.notes.getAll();
    final result = await Future.wait([imageMetaTDOsFuture, noteTODsFuture]);
    final imageMetaTDOs = result[0] as List<ImageMetaTDO>;
    final noteTODs = result[1] as List<NoteTDO>;
    return (imageMetaTDOs, noteTODs);
  }

  final _dataFuture = _loadData();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _dataFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.hasError) {
          final message = messageFromError(snapshot.error, "Failed to load tags");
          return Center(
            child: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          );
        }
        final (imageMetaTDOs, noteTODs) = snapshot.data!;
        return Game(
          imageMetaTDOs: imageMetaTDOs,
          noteTODs: noteTODs,
        );
      },
    );
  }
}

class Game extends StatefulWidget {
  const Game({
    super.key,
    required this.imageMetaTDOs,
    required this.noteTODs,
  });

  final List<ImageMetaTDO> imageMetaTDOs;
  final List<NoteTDO> noteTODs;

  @override
  State<Game> createState() => _GameState();
}

class _GameState extends State<Game> {
  Future<(ui.Image, AnnotationBoundaryBox, String)> _loadRandomData() async {
    var randomIndex = Random().nextInt(widget.imageMetaTDOs.length);
    final imageMetaTDO = widget.imageMetaTDOs[randomIndex];
    final imageFuture = Database.instance.images.get(imageMetaTDO).then((bytes) => decodeImageFromList(bytes));
    final annotationTDOsFuture = Database.instance.annotations.getAnnotationsForImage(imageMetaTDO);
    final result = await Future.wait([imageFuture, annotationTDOsFuture]);
    final image = result[0] as ui.Image;
    final annotationTDOs = (result[1] as List<AnnotationTDO>).where((annotationTDO) => annotationTDO.bbox != null).toList();
    randomIndex = Random().nextInt(annotationTDOs.length);
    final annotationTDO = annotationTDOs[randomIndex];
    final noteTDO = widget.noteTODs.firstWhere((noteTDO) => noteTDO.id == annotationTDO.annotationId);
    return (image, annotationTDO.bbox!, noteTDO.text);
  }

  @override
  initState() {
    super.initState();
    _dataFuture = _loadRandomData();
  }

  Future<(ui.Image, AnnotationBoundaryBox, String)>? _dataFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _dataFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Center(child: CircularProgressIndicator());
        }
        if (snapshot.hasError) {
          final message = messageFromError(snapshot.error, "Failed to data");
          return Center(
            child: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          );
        }
        final (image, boundaryBox, noteText) = snapshot.data!;
        final imageWithRectangle = ImageWithRectangles(
          image: image,
          rectangles: [
            Rect.fromLTWH(
              boundaryBox.x.toDouble(),
              boundaryBox.y.toDouble(),
              boundaryBox.width.toDouble(),
              boundaryBox.height.toDouble(),
            )
          ],
        );
        final nextDataFuture = _loadRandomData();
        final correctButton = FloatingActionButton.large(
          child: Text(noteText),
          onPressed: () {
            setState(() {
              _dataFuture = nextDataFuture;
            });
          },
        );
        final incorrectNotes = widget.noteTODs.where((noteTDO) => noteTDO.text != noteText).toList();
        final incorrectButton = FloatingActionButton.large(
          child: Text(incorrectNotes[Random().nextInt(incorrectNotes.length)].text),
          onPressed: () async {
            await showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: const Text("Incorrect"),
                  content: Text("The correct answer is $noteText"),
                  actions: [
                    TextButton(
                      child: const Text("OK"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
            setState(() {
              _dataFuture = nextDataFuture;
            });
          },
        );
        final buttons = [correctButton, incorrectButton]..shuffle();
        return Stack(
          children: [
            imageWithRectangle,
            Column(
              children: [
                Expanded(child: Container()),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: buttons,
                  ),
                ),
              ],
            )
          ],
        );
      },
    );
  }
}
