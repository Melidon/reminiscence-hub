import "dart:ui" as ui show Image;
import "package:flutter/material.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/image_annotator.dart";
import "package:reminiscence_hub/services/database.dart";
import "package:reminiscence_hub/utility.dart";

class ImageDetailsView extends StatefulWidget {
  const ImageDetailsView(this.imageMetaTDO, this.image, {super.key});

  final ImageMetaTDO imageMetaTDO;
  final ui.Image image;

  @override
  State<ImageDetailsView> createState() => _ImageDetailsViewState();
}

class _ImageDetailsViewState extends State<ImageDetailsView> {
  late final _annotationTDOListFuture = Database.instance.annotations.getAnnotationsForImage(widget.imageMetaTDO);
  late final _noteTDOMapFuture = _annotationTDOListFuture.then((annotationTDOs) {
    final entriesFuture = Future.wait(annotationTDOs
        .where((annotationTDO) => annotationTDO.type == AnnotationType.text)
        .map((annotationTDO) => Database.instance.notes.get(annotationTDO.annotationId).then((noteTDO) => MapEntry(annotationTDO, noteTDO))));
    final mapFuture = entriesFuture.then((entries) => Map.fromEntries(entries));
    return mapFuture;
  });
  final noteTDOListFuture = Database.instance.notes.getAll();

  Map<AnnotationTDO, NoteTDO>? _noteTDOMap;

  Future<void> _addNote(Rect rect) async {
    final noteTDOList = await noteTDOListFuture;
    if (!context.mounted) {
      return;
    }
    final noteTDO = await showDialog<NoteTDO>(
      context: context,
      builder: (context) {
        NoteTDO? selectedNoteTDO;
        return AlertDialog(
          title: const Text("Add note"),
          content: DropdownButtonFormField<NoteTDO>(
            value: selectedNoteTDO,
            onChanged: (NoteTDO? newValue) {
              selectedNoteTDO = newValue;
            },
            items: noteTDOList.map<DropdownMenuItem<NoteTDO>>((NoteTDO noteTDO) {
              return DropdownMenuItem<NoteTDO>(
                value: noteTDO,
                child: Text(noteTDO.text), // Assuming noteTDO has a property 'text'
              );
            }).toList(),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop<NoteTDO>(null);
              },
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop<NoteTDO>(selectedNoteTDO);
              },
              child: const Text("Add"),
            ),
          ],
        );
      },
    );
    if (noteTDO == null) {
      return;
    }
    try {
      final annotationTDO = await Database.instance.annotations.addNoteForImage(
        noteTDO: noteTDO,
        boundaryBox: AnnotationBoundaryBox(x: rect.left, y: rect.top, width: rect.width, height: rect.height),
        imageMetaTDO: widget.imageMetaTDO,
      );
      setState(() {
        _noteTDOMap![annotationTDO] = noteTDO;
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to add $noteTDO.text");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  Future<void> _deleteNote(AnnotationTDO annotationTDO) async {
    final result = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("Delete note"),
        content: Text("Are you sure you want to delete ${_noteTDOMap![annotationTDO]!.text}?"),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(false),
            child: const Text("Cancel"),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(true),
            child: const Text("Delete"),
          ),
        ],
      ),
    );
    if (result == null || !result) {
      return;
    }
    try {
      await Database.instance.annotations.delete(annotationTDO.id);
      setState(() {
        _noteTDOMap!.remove(annotationTDO);
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to delete note");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.imageMetaTDO.imageName),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: FutureBuilder(
        future: _noteTDOMapFuture,
        builder: (context, snapshot) {
          final children = <Widget>[];
          children.add(const Row(
            children: [
              Text("Notes:"),
            ],
          ));
          if (snapshot.connectionState != ConnectionState.done) {
            children.add(const Center(child: CircularProgressIndicator()));
          } else if (snapshot.hasError) {
            final message = messageFromError(snapshot.error, "Failed to load notes");
            children.add(Center(
              child: Text(
                message,
                style: TextStyle(color: Theme.of(context).colorScheme.error),
              ),
            ));
          } else {
            _noteTDOMap ??= snapshot.data;
            children.add(
              Column(
                children: _noteTDOMap!.entries.map((entry) {
                  final annotationTDO = entry.key;
                  final noteTDO = entry.value;
                  return ListTile(
                    title: Text(noteTDO.text),
                    trailing: IconButton(
                      onPressed: () async => await _deleteNote(annotationTDO),
                      tooltip: "Delete note",
                      icon: const Icon(Icons.delete),
                    ),
                  );
                }).toList(),
              ),
            );
          }
          children.add(const Divider());
          children.insert(
            0,
            Hero(
              tag: widget.imageMetaTDO.id,
              child: ImageAnnotator(
                image: widget.image,
                rectangles: _noteTDOMap?.keys
                        .map((annotationTDO) => annotationTDO.bbox)
                        .where((boundaryBox) => boundaryBox != null)
                        .cast<AnnotationBoundaryBox>()
                        .map((boundaryBox) => Rect.fromLTWH(
                              boundaryBox.x.toDouble(),
                              boundaryBox.y.toDouble(),
                              boundaryBox.width.toDouble(),
                              boundaryBox.height.toDouble(),
                            )) ??
                    [],
                onRectAdded: _addNote,
              ),
            ),
          );
          children.add(TagsView(imageMetaTDO: widget.imageMetaTDO));
          return ListView(
            children: children,
          );
        },
      ),
    );
  }
}

class TagsView extends StatefulWidget {
  const TagsView({super.key, required this.imageMetaTDO});

  final ImageMetaTDO imageMetaTDO;

  @override
  State<TagsView> createState() => _TagsViewState();
}

class _TagsViewState extends State<TagsView> {
  late final _tagsFuture = Database.instance.images.getTags(widget.imageMetaTDO);
  List<String>? _tags;

  Future<void> _addTag() async {
    final tag = await showDialog<String>(
      context: context,
      builder: (context) {
        final tagController = TextEditingController();
        return AlertDialog(
          title: const Text("Add tag"),
          content: TextField(
            controller: tagController,
            decoration: const InputDecoration(
              labelText: "Tag",
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop<String>(null);
              },
              child: const Text("Cancel"),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop<String>(tagController.text);
              },
              child: const Text("Add"),
            ),
          ],
        );
      },
    );
    if (tag == null) {
      return;
    }
    try {
      final tags = _tags?.toList() ?? [];
      tags.add(tag);
      await Database.instance.images.setTags(widget.imageMetaTDO, tags);
      setState(() {
        _tags = tags;
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to add $tag");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  Future<void> _deleteTag(String tag) async {
    final result = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("Delete tag"),
        content: Text("Are you sure you want to delete $tag?"),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(false),
            child: const Text("Cancel"),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(true),
            child: const Text("Delete"),
          ),
        ],
      ),
    );
    if (result == null || !result) {
      return;
    }
    try {
      final tags = _tags!.toList();
      tags.remove(tag);
      await Database.instance.images.setTags(widget.imageMetaTDO, tags);
      setState(() {
        _tags = tags;
      });
    } catch (exception) {
      if (context.mounted) {
        final message = messageFromError(exception, "Failed to delete $tag");
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              message,
              style: TextStyle(color: Theme.of(context).colorScheme.error),
            ),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            const Text("Tags:"),
            IconButton(
              onPressed: _addTag,
              tooltip: "Add tag",
              icon: const Icon(Icons.add),
            ),
          ],
        ),
        FutureBuilder(
          future: _tagsFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return const Center(child: CircularProgressIndicator());
            }
            if (snapshot.hasError) {
              final message = messageFromError(snapshot.error, "Failed to load tags");
              return Center(
                child: Text(
                  message,
                  style: TextStyle(color: Theme.of(context).colorScheme.error),
                ),
              );
            }
            _tags ??= snapshot.data;
            return Wrap(
              children: _tags!.map((tag) {
                return Chip(
                  label: Text(tag),
                  onDeleted: () async => await _deleteTag(tag),
                );
              }).toList(),
            );
          },
        ),
      ],
    );
  }
}
