import "package:flutter/material.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/views/auth_gate.dart";
import "package:reminiscence_hub/views/games_view.dart";
import "package:reminiscence_hub/views/image_list_view.dart";
import "package:reminiscence_hub/views/notes_view.dart";

void main() {
  runApp(const ReminiscenceHubApp());
}

class ReminiscenceHubApp extends StatelessWidget {
  const ReminiscenceHubApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Reminiscence Hub",
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
      ),
      home: const AuthGate(child: ReminiscenceHub()),
    );
  }
}

class ReminiscenceHub extends StatefulWidget {
  const ReminiscenceHub({super.key});

  @override
  State<ReminiscenceHub> createState() => _ReminiscenceHubState();
}

class _ReminiscenceHubState extends State<ReminiscenceHub> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reminiscence Hub"),
        actions: [
          IconButton(
            onPressed: Auth.instance.signOut,
            tooltip: "Sign out",
            icon: const Icon(Icons.logout),
          ),
        ],
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.games),
            label: "Games",
          ),
          NavigationDestination(
            icon: Icon(Icons.image),
            label: "Images",
          ),
          NavigationDestination(
            icon: Icon(Icons.notes),
            label: "Notes",
          ),
        ],
        selectedIndex: _selectedIndex,
        onDestinationSelected: (index) => setState(() => _selectedIndex = index),
      ),
      body: const [GamesView(), ImageListView(), NotesView()][_selectedIndex],
    );
  }
}
