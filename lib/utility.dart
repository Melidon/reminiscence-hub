import "dart:convert";

import "package:reminiscence_hub/generated/openapi.swagger.dart";

Object typedError(Object? error) {
  if (error == null) {
    return Exception("Unknown error");
  }
  if (error is! String) {
    return error;
  }
  try {
    final Map<String, dynamic> json = jsonDecode(error);
    final errorResponse = ErrorResponse.fromJson(json);
    return errorResponse;
  } catch (_) {}
  try {
    final List<dynamic> jsonList = jsonDecode(error);
    // TODO: Remove this hack when backend is fixed
    for (final json in jsonList) {
      json["ctx"] ??= Object();
      json["type_"] ??= json["type"];
    }
    final validationErrorModels = jsonList.map((json) => ValidationErrorModel.fromJson(json)).toList();
    return validationErrorModels;
  } catch (_) {}
  return error;
}

String messageFromError(dynamic error, String fallbackMessage) {
  final message = switch (error) {
    ErrorResponse() => error.source?.error,
    List<ValidationErrorModel>() => error[0].msg,
    String() => error,
    _ => null,
  };
  return message ?? fallbackMessage;
}
