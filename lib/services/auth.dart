import "dart:async";

import "package:reminiscence_hub/config/backend.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/utility.dart";

class Auth {
  static Auth get instance => _instance;
  static final Auth _instance = Auth._();
  Auth._();

  Future<void> _setAccessToken(String? accessToken) {
    return AuthRepository.instance.setAccessToken(accessToken);
  }

  Future<String?> _getRefreshToken() {
    return AuthRepository.instance.getRefreshToken();
  }

  Future<void> _setRefreshToken(String? refreshToken) async {
    await AuthRepository.instance.setRefreshToken(refreshToken);
    _authStateController.add(refreshToken != null);
  }

  late final StreamController<bool> _authStateController = StreamController<bool>.broadcast(
    onListen: () async {
      final tokenModel = await _getRefreshToken();
      _authStateController.add(tokenModel != null);
    },
  );

  Stream<bool> authStateChanges() {
    return _authStateController.stream;
  }

  Future<void> createUserWithEmailAndPassword(String email, String password) async {
    final response = await Backend.instance.apiV1UsersRegistryPost(
      body: UserBody(email: email, password: password),
    );
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    await signInWithEmailAndPassword(email, password);
  }

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    final response = await Backend.instance.apiV1UsersLoginPost(
      body: UserBody(email: email, password: password),
    );
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    await Future.wait([
      _setAccessToken(response.body!.source!.accessToken),
      _setRefreshToken(response.body!.source!.refreshToken),
    ]);
  }

  Future<void> refresh() async {
    final refreshToken = await _getRefreshToken();
    if (refreshToken == null) {
      throw Exception("User is not signed in");
    }
    final response = await Backend.instance.apiV1UsersRefreshPost();
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    await _setAccessToken(response.body!.source!.accessToken);
  }

  Future<void> signOut() async {
    await Future.wait([
      _setAccessToken(null),
      _setRefreshToken(null),
    ]);
  }
}
