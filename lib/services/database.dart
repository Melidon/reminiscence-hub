import "dart:convert";
import "dart:io";
import "dart:typed_data";

import "package:http/http.dart" as http;
import "package:http_parser/http_parser.dart";
import "package:image_picker/image_picker.dart";
import "package:reminiscence_hub/config/backend.dart";
import "package:reminiscence_hub/generated/openapi.swagger.dart";
import "package:reminiscence_hub/services/auth.dart";
import "package:reminiscence_hub/utility.dart";

class Database {
  static Database get instance => _instance;
  static final _instance = Database._();
  Database._();

  Images get images => _images;
  Annotations get annotations => _annotations;
  Notes get notes => _notes;

  final _images = Images._();
  final _annotations = Annotations._();
  final _notes = Notes._();
}

class Images {
  Images._();

  // TODO: Handle errors
  Future<ImageMetaTDO> add(XFile image) async {
    final url = Uri.parse("${Backend.instance.client.baseUrl}/api/v1/images");
    final request = http.MultipartRequest("POST", url);
    final bytes = await image.readAsBytes();
    final mimeType = MediaType("image", image.name.split(".").last);
    final multipartFile = http.MultipartFile.fromBytes("file", bytes, filename: image.name, contentType: mimeType);
    request.files.add(multipartFile);
    final accessToken = await AuthRepository.instance.getAccessToken();
    request.headers[HttpHeaders.authorizationHeader] = "Bearer $accessToken";
    var response = await request.send();
    if (response.statusCode == HttpStatus.unauthorized) {
      await Auth.instance.refresh();
      final newAccessToken = await AuthRepository.instance.getAccessToken();
      request.headers[HttpHeaders.authorizationHeader] = "Bearer $newAccessToken";
      response = await request.send();
    }
    final responseString = await response.stream.bytesToString();
    final json = jsonDecode(responseString);
    final imageMetaTDO = ImageMetaTDO.fromJson(json["source"]);
    return imageMetaTDO;
  }

  Future<Uint8List> get(ImageMetaTDO imageMetaTDO) async {
    final baseUrl = Backend.instance.client.baseUrl;
    const apiKey = "iy35ADaK2eBk3MMhHXGly8PHMNjwaQyPW9zl475i9tJAAn5DjiSJovxbBxLD61dF";
    final imageUrl = "$baseUrl/api/v1/images/${imageMetaTDO.id}/download?api_key=$apiKey";
    final response = await http.get(Uri.parse(imageUrl));
    if (response.statusCode != HttpStatus.ok) {
      throw Exception("Failed to get image");
    }
    return response.bodyBytes;
  }

  Future<List<ImageMetaTDO>> getAllMeta() async {
    final response = await Backend.instance.apiV1ImagesGet();
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!.images;
  }

  Future<void> delete(ImageMetaTDO imageMetaTDO) async {
    final response = await Backend.instance.apiV1ImagesIdDelete(id: imageMetaTDO.id);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
  }

  Future<List<String>> getTags(ImageMetaTDO imageMetaTDO) async {
    final response = await Backend.instance.apiV1ImagesIdTagsGet(id: imageMetaTDO.id);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!.tags;
  }

  Future<void> setTags(ImageMetaTDO imageMetaTDO, List<String> tags) async {
    final response = await Backend.instance.apiV1ImagesIdTagsPost(id: imageMetaTDO.id, body: ImageMetaBody(tags: tags));
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
  }
}

class Annotations {
  Annotations._();

  Future<AnnotationTDO> addNoteForImage({
    required NoteTDO noteTDO,
    AnnotationBoundaryBox? boundaryBox,
    required ImageMetaTDO imageMetaTDO,
  }) async {
    final response = await Backend.instance.apiV1AnnotationsPost(
      body: AnnotationBody(
        annotationId: noteTDO.id,
        bbox: boundaryBox,
        imageId: imageMetaTDO.id,
        type: AnnotationType.text
      ),
    );
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!;
  }

  Future<List<AnnotationTDO>> getAnnotationsForImage(ImageMetaTDO imageMetaTDO) async {
    final response = await Backend.instance.apiV1AnnotationsGet();
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    final annotations = response.body!.source!.annotations;
    final annotationsForImage = annotations.where((noteAnnotation) => noteAnnotation.imageId == imageMetaTDO.id);
    return annotationsForImage.toList();
  }

  Future<void> delete(String id) async {
    final response = await Backend.instance.apiV1AnnotationsIdDelete(id: id);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
  }
}

class Notes {
  Notes._();

  Future<NoteTDO> add(NoteBody noteBody) async {
    final response = await Backend.instance.apiV1NotesPost(body: noteBody);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!;
  }

  Future<NoteTDO> get(String id) async {
    final response = await Backend.instance.apiV1NotesIdGet(id: id);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!;
  }

  Future<List<NoteTDO>> getAll() async {
    final response = await Backend.instance.apiV1NotesGet();
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!.notes;
  }

  Future<NoteTDO> update(String id, NoteBody noteBody) async {
    final response = await Backend.instance.apiV1NotesIdPut(id: id, body: noteBody);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
    return response.body!.source!;
  }

  Future<void> delete(String id) async {
    final response = await Backend.instance.apiV1NotesIdDelete(id: id);
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
  }

  Future<void> deleteAll() async {
    final response = await Backend.instance.apiV1NotesDelete();
    if (!response.isSuccessful) {
      throw typedError(response.error);
    }
  }
}
