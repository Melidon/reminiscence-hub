import 'dart:ui' as ui show Image;
import 'package:flutter/material.dart';

typedef RectAddedCallback = Future<void> Function(Rect rect);

class ImageAnnotator extends StatefulWidget {
  const ImageAnnotator({
    super.key,
    required this.image,
    required this.rectangles,
    required this.onRectAdded,
  });

  final ui.Image image;
  final Iterable<Rect> rectangles;
  final RectAddedCallback onRectAdded;

  @override
  State<ImageAnnotator> createState() => _ImageAnnotatorState();
}

class _ImageAnnotatorState extends State<ImageAnnotator> {
  Offset? _start;
  Offset? _end;

  @override
  Widget build(BuildContext context) {
    final rectangles = widget.rectangles.toList();
    if (_start != null && _end != null) {
      rectangles.add(Rect.fromPoints(_start!, _end!));
    }
    return FittedBox(
      child: GestureDetector(
        child: SizedBox(
          width: widget.image.width.toDouble(),
          height: widget.image.height.toDouble(),
          child: CustomPaint(
            painter: _ImageWithRectanglesPainter(widget.image, rectangles),
          ),
        ),
        onPanStart: (details) {
          setState(() {
            _start = details.localPosition;
            _end = null;
          });
        },
        onPanUpdate: (details) {
          if (_start == null) {
            return;
          }
          setState(() {
            _end = details.localPosition;
          });
        },
        onPanEnd: (details) async {
          if (_start == null || _end == null) {
            return;
          }
          await widget.onRectAdded(Rect.fromPoints(_start!, _end!));
          setState(() {
            _start = null;
            _end = null;
          });
        },
      ),
    );
  }
}

class ImageWithRectangles extends StatelessWidget {
  const ImageWithRectangles({
    super.key,
    required this.image,
    required this.rectangles,
  });

  final ui.Image image;
  final Iterable<Rect> rectangles;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: SizedBox(
        width: image.width.toDouble(),
        height: image.height.toDouble(),
        child: CustomPaint(
          painter: _ImageWithRectanglesPainter(image, rectangles),
        ),
      ),
    );
  }
}

class _ImageWithRectanglesPainter extends CustomPainter {
  const _ImageWithRectanglesPainter(
    this.image,
    this.rectangles,
  );

  final ui.Image image;
  final Iterable<Rect> rectangles;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawImage(image, Offset.zero, Paint());
    final paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8;
    for (final rectangle in rectangles) {
      canvas.drawRect(rectangle, paint);
    }
  }

  @override
  bool shouldRepaint(_ImageWithRectanglesPainter oldDelegate) {
    return oldDelegate.image != image || oldDelegate.rectangles != rectangles;
  }
}
