#!/bin/bash

# Download http://vm.niif.cloud.bme.hu:4916/openapi/openapi.json
rm -rf lib/swaggers && \
mkdir lib/swaggers && \
cd lib/swaggers && \
wget http://vm.niif.cloud.bme.hu:4916/openapi/openapi.json && \
cd ../.. && \

# Generate client
rm -rf lib/generated && \
dart run build_runner build
